import torch
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import Dataset, DataLoader
import os
from PIL import Image
import torchvision.transforms.functional as TF
import numpy as np
from config import ALPHABET


class OCR_DataLoader:
    def __init__(self, root_dir, batch_size=1, difficulty="easy"):
        batch_size = batch_size
        # TODO change to train valid and test, for overfitting purposes all same
        dataset_train = OCR_Dataset(root_dir, "devel." + difficulty, batch_size)
        dataset_val = OCR_Dataset(root_dir, "valid." + difficulty, batch_size)
        dataset_test = OCR_Dataset(root_dir, "devel." + difficulty, batch_size)

        if torch.cuda.is_available():
            #TODO shuffle to true
            self.train_data = DataLoader(dataset_train, batch_size=batch_size, collate_fn=dataset_train.collate,
                                         pin_memory=True, num_workers=4, shuffle=False)
            self.val_data = DataLoader(dataset_val, batch_size=batch_size, collate_fn=dataset_val.collate,
                                       pin_memory=True, num_workers=4, shuffle=False)
            self.test_data = DataLoader(dataset_test, batch_size=batch_size, collate_fn=dataset_test.collate,
                                        pin_memory=True, num_workers=4, shuffle=False)
        else:
            self.train_data = DataLoader(dataset_train, batch_size=batch_size, collate_fn=dataset_train.collate)
            self.val_data = DataLoader(dataset_val, batch_size=batch_size, collate_fn=dataset_val.collate)
            self.test_data = DataLoader(dataset_test, batch_size=batch_size, collate_fn=dataset_test.collate)


class OCR_Dataset(Dataset):
    def __init__(self, root_dir, labels_file, batch_size):
        self.root_dir = root_dir
        self.batch_size = batch_size
        self.files = open(root_dir + labels_file, 'r')
        self.image_files = [file.split(" ", 1) for file in self.files.readlines()]
        self.files, self.labels = list(zip(*self.image_files))

    def labels_to_numpy(self, text):
        values_in_int = []
        values_in_int.append([ALPHABET[c] for c in text])
        return np.array(values_in_int)[0]

    def __len__(self):
        return len(self.files)

    def __getitem__(self, index):
        # TODO testing samples dont have label, handle it.
        label = self.labels[index]
        label = self.labels_to_numpy(label.strip())

        img = Image.open(os.path.join(self.root_dir, "lines", self.files[index]))
        width, height = img.size
        img = img.resize((width, 32))
        img = TF.to_tensor(img)

        img_width = img.shape[2]

        return img, img_width, torch.tensor(label), len(label)

    def collate(self, batch):
        image = pad_sequence([item[0].permute(2, 1, 0) for item in batch])

        image = image.permute(1, 3, 2, 0)
        # Image: B x Ch x H x W

        images_widths = torch.tensor([item[1] for item in batch])
        labels = pad_sequence([item[2] for item in batch])
        labels = labels.permute(1, 0)
        label_lens = torch.tensor([item[3] for item in batch])
        return image, images_widths, labels, label_lens
