import numpy as np
import matplotlib.pyplot as plt
import os
from shutil import copyfile
import sys
lines = []
with open(sys.argv[1]) as target:
    for line in target.read().splitlines():
        label = line.split(" ", 1)[1]
        file = line.split(" ", 1)[0]
        lines.append(len(label))
        if len(label) < int(sys.argv[2]):
            print(file, label)

#_ = plt.hist(lines, bins='auto')  # arguments are passed to np.histogram
#plt.title("Histogram with 'auto' bins")
#plt.show()
