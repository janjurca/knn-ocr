module add cudnn-7.6.4-cuda10.1
module add python-3.6.2-gcc
cd $SCRATCHDIR || exit 1
mkdir temp
TMPDIR=$(pwd)/temp
export TMPDIR

pip3.6 install torch==1.4.0 torchvision==0.5.0 --cache-dir $(pwd)/cache --root $(pwd) --user
pip3.6 install python-Levenshtein --cache-dir $(pwd)/cache --root $(pwd) --user
pip3.6 install lmdb --cache-dir $(pwd)/cache --root $(pwd) --user
pip3.6 install opencv-python --cache-dir $(pwd)/cache --root $(pwd) --user
PYTHONPATH=$(pwd)/storage/praha1/home/xjurca08/.local/lib/python3.6/site-packages/:$PYTHONPATH
export PYTHONPATH

mkdir dataset
cd dataset
wget https://www.fit.vutbr.cz/~ikiss/b-mod/b-mod_lines.zip
unzip b-mod_lines.zip
cd ..

git clone git@bitbucket.org:josef_kadlecek/knn-ocr.git
cd knn-ocr/
python3 train.py ../dataset/ 8
