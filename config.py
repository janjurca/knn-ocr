import torch
import os
from datetime import datetime

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

BLANK = 'ů'
# TODO rework - #is in dataset, alo upper and lower case are not the same.
ALPHABET = {
            'ů': 0, '"': 1, '#': 2, '%': 3, '\'': 4, '(': 5, ')': 6, '*': 7, '+': 8, ',': 9,
            '-': 10, '.': 11, '/': 12, ':': 13, ';': 14, '=': 15, '?': 16, '@': 17, '[': 18,
            ']': 19, '_': 20, '{': 21, '|': 22, '}': 23, '~': 24, '$': 25, 'a': 26, 'A': 27,
            'b': 28, 'B': 29, 'c': 30, 'C': 31, 'd': 32, 'D': 33, 'e': 34, 'E': 35, 'f': 36,
            'F': 37, 'g': 38, 'G': 39, 'h': 40, 'H': 41, 'i': 42, 'I': 43, 'j': 44, 'J': 45,
            'k': 46, 'K': 47, 'l': 48, 'L': 49, 'm': 50, 'M': 51, 'n': 52, 'N': 53, 'o': 54,
            'O': 55, 'p': 56, 'P': 57, 'q': 58, 'Q': 59, 'r': 60, 'R': 61, 's': 62, 'S': 63,
            't': 64, 'T': 65, 'u': 66, 'U': 67, 'v': 68, 'V': 69, 'w': 70, 'W': 71, 'x': 72,
            'X': 73, 'y': 74, 'Y': 75, 'z': 76, 'Z': 77, '0': 78, '1': 79, '2': 80, '3': 81,
            '4': 82, '5': 83, '6': 84, '7': 85, '8': 86, '9': 87, ' ': 88, '!': 89, '\\': 90
        }


class Config():
    def __init__(self, dataset_root, batch_size, epochs, difficulty, learning_rate):
        self.dataset_root = dataset_root
        self.batch_size = batch_size
        self.alphabet = ALPHABET
        self.learning_rate = learning_rate
        self.device = DEVICE
        self.log_epoch = 1
        self.difficulty = difficulty
        self.epochs = epochs
        self.experiment_folder = os.path.join(dataset_root, str(format(datetime.now(), '%d.%m.%H.%M')))
