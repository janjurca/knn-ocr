import math
import os

import torch
import numpy as np
from torch import nn, optim
from torch.autograd import Variable

from config import Config
from dataloader import OCR_DataLoader
from model import OCR_Model, CRNN, CNN
from testing_and_validation import validate_model, data_to_device, update_cer, avg_loss
import argparse


def load_checkpoint(model, optimizer, filename='checkpoint.pth.tar'):
    # Note: Input model & optimizer should be pre-defined.  This routine only updates their states.
    start_epoch = 0
    if os.path.isfile(filename):
        print("=> loading checkpoint '{}'".format(filename))
        checkpoint = torch.load(filename)
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        print("=> loaded checkpoint '{}' (epoch {})".format(filename, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(filename))

    return model, optimizer, start_epoch


def init_model(config, selected_model, model_path=None):
    model = selected_model(len(config.alphabet))
    optimizer = optim.Adam(model.parameters(), lr=config.learning_rate)
    model.to(config.device)

    if model_path:
        model, optimizer, epoch = load_checkpoint(model, optimizer, model_path)

    return model, optimizer


def train_model(model, dl, config, optimizer):
    #torch.manual_seed(0)
    loss_fn = nn.CTCLoss().to(config.device)
    print("Training started...")

    for epoch in range(config.epochs):
        training_losses = []
        cer_history = np.array([])

        for batch_num, (image, image_size, label, label_len) in enumerate(dl.train_data):
            image, image_width, label, label_len = data_to_device(image, image_size, label, label_len)
            optimizer.zero_grad()
            model.train()

            model_output = model(image)
            input_lens = Variable(torch.LongTensor([model_output.size(0)] * model_output.size(1)))
            loss = loss_fn(model_output, label, input_lens, label_len)

            model.eval()
            model_output = model(image)
            curr_loss = loss.data.cpu().numpy()
            cer_history = update_cer(cer_history, model_output, label)

            if np.isnan(curr_loss) or math.isinf(curr_loss):
                print("Alert nans or inf!!!")
            else:
                training_losses.append(curr_loss)
                loss.backward()
                # TODO: Check if this is necceary
                # torch.nn.utils.clip_grad_norm_(model.parameters(), 0.25)
                optimizer.step()
                if batch_num % 500 == 0:
                    training_loss = avg_loss(training_losses)
                    print(f"Epoch {epoch} Batch {batch_num} Training loss: {training_loss} Training CER {np.mean(cer_history)}")

        training_loss = avg_loss(training_losses)
        validation_loss, validation_cer = validate_model(dl, model, loss_fn, config)
        print(f"Epoch {epoch} Training loss: {training_loss} Training CER {np.mean(cer_history)} Validation loss: {validation_loss} Validation CER {validation_cer}")

        state = {'epoch': epoch + 1, 'state_dict': model.state_dict(), 'optimizer': optimizer.state_dict()}
        torch.save(state, "net.torch." + str(epoch))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", action="store", type=str, help="Dataset root dir")
    parser.add_argument("--batch", action="store", type=int, help="Batch size")
    parser.add_argument("--epochs", action="store", type=int, default=100, help="Epoch count")
    parser.add_argument("--lr", action="store", type=float, default=0.0001, help="Epoch count")
    parser.add_argument("--difficulty", action="store", type=str, default="easy", help="difficulty string, manifest is choosen accordingly")
    parser.add_argument("--load", action="store", type=str, default=None, help="Saved torch model file")
    parser.add_argument("--model", action="store", type=str, default="default", help="model identificator")
    args = parser.parse_args()

    config = Config(dataset_root=args.dataset, batch_size=args.batch, epochs=args.epochs, difficulty=args.difficulty,learning_rate=args.lr)
    dl = OCR_DataLoader(config.dataset_root, config.batch_size, config.difficulty)
    models = {
        "default": OCR_Model,
        "ocr": OCR_Model,
        "crnn": CRNN,
        "cnn": CNN,

    }

    model, optimizer = init_model(config, models[args.model],args.load)

    train_model(model, dl, config, optimizer)


if __name__ == "__main__":
    main()
