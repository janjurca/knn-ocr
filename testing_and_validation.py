import torch
import numpy as np

import Levenshtein
from config import DEVICE, ALPHABET, BLANK
from torch.autograd import Variable


def avg_loss(losses):
    return np.nanmean(np.array([x for x in losses]))


def update_cer(cer, model_output, label, to_print=False):
    lines = ""
    for i in range(model_output.size()[1]):
        gt, out = decode(model_output[:, i, :], label[i])
        cer = np.append(cer, Levenshtein.distance(gt, out) / len(gt) * 100)
        if to_print:
            lines += ">" + gt + "<  >" + out + "<\n"
    if to_print:
        with open("netvalid.outputs", 'w') as fp:
            fp.write(lines)
    return cer


def data_to_device(image, image_width, label, label_len):
    image = image.to(DEVICE)
    image_width = image_width.to(DEVICE)
    label = label.to(DEVICE)
    label_len = label_len.to(DEVICE)
    return image, image_width, label, label_len


def decode(model_output, ground):
    model_output = model_output.to("cpu")
    alphabet_to_dix = dict((v, k) for k, v in ALPHABET.items())
    decoded = []

    for frame in model_output:
        frame = frame.detach().numpy()
        idx = frame.argmax()
        decoded.append(alphabet_to_dix[idx])

    decoded = " ".join("".join(decoded).split())

    res = [' ']
    for i, c in enumerate(decoded):
        if res[-1] != c:
            res.append(c)

    out = ("".join(res[1:]))
    out = out.replace(BLANK, "")

    gt = "Something went wrong."
    if ground.type() is not None:
        ground = ground.to("cpu")
        ground = ground.numpy().squeeze()
        label_decoded = [alphabet_to_dix[idx] for idx in ground]
        gt = ("".join(label_decoded))
        gt = gt.replace(BLANK, "")
    return gt, out


def validate_model(dl, model, loss_fn, config):
    torch.no_grad()
    model.eval()
    losses = []
    cer_history = np.array([])
    # Validate on validation data
    for i, (image, image_width, label, label_len) in enumerate(dl.val_data):
        image, image_width, label, label_len = data_to_device(image, image_width, label, label_len)

        model_output = model(image)
        input_lens = Variable(torch.LongTensor([model_output.size(0)] * model_output.size(1)))
        loss = loss_fn(model_output, label, input_lens, label_len)
        losses.append(loss.data.cpu().numpy())
        to_print = False
        if i*config.batch_size < 50:
            to_print = True
        cer_history = update_cer(cer_history, model_output, label, to_print)
    model.train()
    return avg_loss(losses), np.mean(cer_history)
