import torch.nn as nn
import torch
import torchvision.models as models


class OCR_Model(nn.Module):
    def __init__(self, embedding_dim, rnn_type=nn.LSTM, rnn_size=512, dropout=0.2):
        super(OCR_Model, self).__init__()
        self.rnn_size = rnn_size

        self.conv_layers = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3, padding=1),
            nn.BatchNorm2d(32),
            nn.LeakyReLU(negative_slope=0.05),
            nn.MaxPool2d(2),

            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(negative_slope=0.05),
            nn.MaxPool2d(2),

            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(negative_slope=0.05),
            nn.MaxPool2d(2),

            nn.Conv2d(in_channels=128, out_channels=rnn_size, kernel_size=3, padding=1),
            nn.BatchNorm2d(rnn_size),
            nn.LeakyReLU(negative_slope=0.05),
            nn.MaxPool2d(2),
        )

        self.rnn1 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, bidirectional=True)
        self.rnn2 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, bidirectional=True)
        self.rnn3 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, bidirectional=True)
        self.rnn4 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, bidirectional=True)

        self.drop1 = nn.Dropout(dropout)
        self.drop2 = nn.Dropout(dropout)

        self.last_conv = nn.Sequential(
            nn.BatchNorm1d(self.rnn_size),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Conv1d(in_channels=self.rnn_size, out_channels=512, kernel_size=3, padding=1),
            nn.BatchNorm1d(512),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Conv1d(in_channels=512, out_channels=embedding_dim, kernel_size=3, padding=1),
            nn.LogSoftmax(dim=1)
        )

    def forward(self, x):
        # Input: Tensor of size [BATCH, 3(ch), 48(height), n(width)])
        batch_size, c, h, w = x.shape
        x = self.conv_layers(x)
        x = x.view(-1, batch_size, self.rnn_size)
        # seq_len, batch, input_size

        x, _ = self.rnn1(x)

        x, _ = self.rnn2(x)

        x, _ = self.rnn3(x)

        x, _ = self.rnn4(x)

        x = x.permute(1, 2, 0)
        x = self.last_conv(x)

        x = x.permute(2, 0, 1)

        # Output must be Tensor of size (seq_len , batch_size, n_alphabet+1)
        return x


class CRNN(nn.Module):
    def __init__(self, embedding_dim, rnn_type=nn.LSTM, rnn_size=512, dropout=0.2):
        super(CRNN, self).__init__()
        self.rnn_size = rnn_size

        pret = models.vgg13(pretrained=True)

        self.conv_layers = pret.features[:21]

        self.rnn1 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, bidirectional=True)
        self.rnn2 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, bidirectional=True)
        self.rnn3 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, bidirectional=True)
        self.rnn4 = rnn_type(input_size=rnn_size, hidden_size=rnn_size // 2, bidirectional=True)


        self.last_conv = nn.Sequential(
            nn.BatchNorm1d(self.rnn_size),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Conv1d(in_channels=self.rnn_size, out_channels=512, kernel_size=3, padding=1),
            nn.BatchNorm1d(512),
            nn.LeakyReLU(negative_slope=0.05),
            nn.Conv1d(in_channels=512, out_channels=embedding_dim, kernel_size=3, padding=1),
            nn.LogSoftmax(dim=1)
        )

    def forward(self, x):
        # Input: Tensor of size [BATCH, 3(ch), 48(height), n(width)])
        batch_size, c, h, w = x.shape
        x = self.conv_layers(x)

        x = x.view(-1, batch_size, self.rnn_size)
        # seq_len, batch, input_size

        x, _ = self.rnn1(x)

        x = x.permute(1, 2, 0)
        x = self.last_conv(x)

        x = x.permute(2, 0, 1)

        # Output must be Tensor of size (seq_len , batch_size, n_alphabet+1)
        return x


class CNN(nn.Module):
    def __init__(self, embedding_dim, rnn_type=nn.LSTM, rnn_size=512, dropout=0.2):
        super(CNN, self).__init__()
        self.rnn_size = rnn_size

        #pret = models.vgg13(pretrained=False)
        #self.conv_layers = pret.features[:21]

        self.conv_layers = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2),

            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2),

            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, padding=1, stride=(3, 1)),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.MaxPool2d(2),
        )

        self.rnn = rnn_type(input_size=128, hidden_size=128, bidirectional=True)
        self.conv = nn.Conv1d(in_channels=256, out_channels=embedding_dim, kernel_size=3, padding=1)
        self.soft = nn.LogSoftmax(dim=1)


    def forward(self, x):
        # Input: Tensor of size [BATCH, 3(ch), 48(height), n(width)])
        x = self.conv_layers(x)
        x = x.squeeze()
        x = x.permute(2, 0, 1)
        # seq_len, batch, input_size
        x, _ = self.rnn(x)

        x = x.permute(1, 2, 0)
        x = self.conv(x)
        x = self.soft(x)

        x = x.permute(2, 0, 1)

        #print(x.size())
        #exit(0)


        # Output must be Tensor of size (seq_len , batch_size, n_alphabet+1)
        return x
