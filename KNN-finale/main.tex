\documentclass[11pt , a4paper , twocolumn]{article}

\usepackage[left=1.5cm,text={18cm, 25cm},top=2.5cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage[IL2]{fontenc}
\usepackage{amsthm, amssymb, amsmath}
\usepackage[multiple]{footmisc}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\usepackage{url}
\def\UrlBreaks{\do\/\do-}
\usepackage{breakurl}
\usepackage[breaklinks]{hyperref}


\usepackage{natbib}
\usepackage{graphicx}
\usepackage{todonotes}
%\usepackage[hidelinks,unicode]{hyperref}
\usepackage{float}
\usepackage{subcaption}
\usepackage{listings}           
\usepackage{hyperref}
\setcitestyle{square}

\usepackage{caption}
\usepackage{subcaption}

\title{KNN - Projekt OCR}
\author{Jan Jurča (xjurca08), Josef Kadleček (xkadle35), Martin Štěpánek (xstepa59)}
\date{}

\begin{document}

\maketitle


Žijeme v době, kdy pokud chceme s informacemi pracovat, je naprosto nezbytné je kategorizovat, třídit a uspořádávat - nic z toho však není možné, pokud jsou data "jen na papíře" - pouze ve vizuální formě. Pro práci s jakýmkoliv textovým dokumentem, poznávací značkou či adresou na obálce je nejdříve potřeba převést vizuální informaci do digitální textové podoby. A právě tím se náš tým zaobíral v rámci této práce.
\\
Tento dokument sestává z návrhu projektu realizující "Optické rozpoznávání znaků", neboli OCR (\textit{Optical Character Recognition}) za pomoci konvolučních neuronových sítí. Nejdříve budou stručně shrnuty existující přístupy k této problematice, krátce porovnány dostupné datové sady a v poslední řadě bude představena konkrétní implementace, která byla v rámci projektu realizována.

\section*{Existující přístupy}
\par
Pro řešení daného problému lze využít několik různých řešení od naivnějších až po velmi komplexní. 
\par
\textbf{Klasifikace slov.} Jedním ze základních řešení je rozpoznání textu po jednotlivých slovech. Vstupem takového systému je tedy výřez jednoho slova a výstupem je vektor hodnot, které představují pravděpodobnosti jednotlivých možných slov. Nevýhodou této sítě by bylo omezení počtu rozpoznávaných slov pouze na definovaná slova. Architektura této sítě by mohla vycházet ze sítě AlexNet\cite{imagenet} nebo z některé její modernější obdoby. 
\par
\textbf{Enkodér-dekodér.} Možným komplexnějším řešením je přístup enkodér-dekodér\cite{encoder-decoder}, který využívá rekurentních vrstev k zakódování vstupu, kterým může být výřez slova, nebo i celého řádku, do jednoho popisného vektoru. Tento popisný vektor se metodou seq2seq dekodéru, kterou je možné vidět na obrázku \ref{fig:ed-image}, zpracuje do formy výsledné sekvence. \\
Hlavní výhodou použití této architektury je možnost přidání \textit{attention}.
Zatímco u přístupu enkodér-dekodér bez mechanismu \textit{attention} dochází při přepisu slov k postupné ztrátě informace (slovo na začátku věty bude již na konci věty „zapomenuto“ nebo bude mít jen pramalý vliv), přidání mechanismu \textit{attention} umožní přístup ke všem předchozím slovům vstupní sekvence, takže každá následující klasifikace může tuto předchozí informaci využít. 
\begin{figure}[H]
\center{\includegraphics[width=\textwidth/2, height=140]{encoder-decoder.png}}
\caption{\label{fig:ed-image} Ilustrace metody Enkodér-dekodér. Převzato z \cite{encoder-decoder} }
\end{figure}

\textbf{CTC - Connectionist Temporal Classification.} Další variantou řešení je využití chybové funkce CTC\cite{ctc-origin}, která může být použita například ve spojení s rekurentní vrstvou \cite{ctc-image}. Konvoluční vrstvou je nejprve vstupní obraz horizontálně podvzorkován a redukován na výšku 1, RNN pak zpracuje tyto extrahované příznaky jako 1D sekvenci. Na konci je sekvence pravděpodobností jednotlivých znaků, která je "synchronizována" se vstupním obrázkem.
\begin{figure}[H]
\center{\includegraphics[width=\textwidth/2]{ctc.png}}
\caption{\label{fig:ctc-image} Samotná CNN zde slouží k extrakci příznaků. Rekurentní část následně zpracovává vstup po jednotlivých sloupcích a vytváří pravděpodobnostní matici, kde řádky odpovídají znakům a sloupce jednotlivým pozicím\cite{ctc-image}}.
\end{figure}

\par
\textbf{Konvolučně rekurentní sítě.} Jak již bylo naznačeno, často používaným řešením\cite{end-to-end-ocr} je využití kombinace konvoluční a rekurentní sítě kde konvoluční část extrahuje příznaky ze vstupního výřezu řádku a rekurentní síť zpracuje sekvenci vyextrahovaných příznaků, jak už bylo vyobrazeno v obrázku \ref{fig:ctc-image}. Obě tyto sítě lze trénovat najednou využitím jedné objektivní funkce\cite{ctc-origin} na datasetu $D={I_i,L_i}$ 
$$
O = -\sum_{I_i,L_i\in D}log (p(L_i|y_i)),
$$
kde $I_i$ jsou vstupní obrázky, $L_i$ jejich anotace a $y_i$ je výstup sítě se vstupem $I_i$.
\par
Protože při určování výsledného řetězce se v rámci CTC pracuje se sekvencí pravděpodobnostních vektorů, ve které neplatí pravidlo, že by jeden vektor popisoval právě jeden znak, je třeba definovat mapovací funkci, která výstupní sekvenci nejpravděpodobnějších znaků přetransformuje na výsledný řetězec. Tato funkce je často realizována tak, že se z dané sekvence nejdříve odstraní opakované symboly a následně se z řetězce odstraní "blank" symboly. Příkladem může být transformace řetězce nejpravděpodobnějších znaků "--a-hh-o--jj-" na výsledný řetězec "ahoj".
\par
Jedná se o supervizované učení, je potřeba mít dostupnou trénovací, testovací a validační datovou sadu, na níž se provádí experimenty. K vyhodnocení, stejně jako ke trénování, je potřeba mít dostupný vstupní výřez slova nebo řádku, a k němu odpovídající přepis. 
Pro vyhodnocení modelu lze využít Levenštejnovu vzdálenost.
\par
\textbf{Levenštejnova vzdálenost.} Jedná se o metriku pro porovnání dvou řetězců, pro jejichž dva prvky je možné
rozhodnout, zda jsou stejné, či nikoliv\cite{Levenshtein}. Výsledkem je skalární hodnota reprezentující nejmenší
počet nutných úprav, kde se může počítat i s jejich cenou, zdrojového řetězce tak, aby se shodoval s cílovým seznamem.
\par
Úspěšnost přepisu lze vyjádřit pomocí metrik \textit{character error rate} (CER - chybovost v písmenech) a \textit{word error rate} (WER - chybovost ve slovech). 
$$
CER =\frac{d_c}{l_c},
$$$$
WER =\frac{d_w}{l_w},
$$
kde $l_c$  a $l_w$ jsou počty znaků, respektive slov ve správném přepisu řádku a $d_c$ a $d_w$ jsou Levenštejnovy vzdálenosti mezi skutečným a vypočítaným přepisem řádku počítané ve znacích, respektive ve slovech.


\section*{Datová sada}

\textbf{Dostupné datové sady.} Datových sad zabývajících se rozpoznáváním textu existuje poměrně velké množství. Dělí se na několik kategorií. Některé se zaměřují na rozpoznávání textu ve scéně (anglicky \textit{scene text}). Sem patří datasety COCO-Text\cite{coco}, DOST\cite{dost} či Chars74K\cite{chars74k}.

Další velkou skupinou jsou datové sady ručně psaného textu. Zde je potřeba zmínit NIST Special Database 19\cite{nist}, která obsahuje 810000 obrázků ručně psaného textu, a to jak samostatných znaků a číslic, tak i souvislého textu. Podmnožina tohoto datasetu má název MNIST\cite{mnist} a obsahuje pouze ručně psané číslice.
Dalšími sadami ručně psaného textu jsou například IAM Handwriting Database\cite{iam} obsahující přes 80 tisíc slov v celých anglických větách, Stanford OCR\cite{stanford} obsahující jednotlivá vyřezaná slova či RIMES\cite{rimes}, který obsahuje celé stránky francouzského textu.

Z pohledu našeho projektu je nejzajímavější poslední skupina - datové sady tištěného textu. V první řadě se jedná o námi použitý dataset \textbf{Brno Mobile OCR}\cite{brno-mobile-ocr}. Ten obsahuje fotografie 2113 unikátních stránek z různých vědeckých článků, pořízených 23 různými mobilními telefony. Celkem se jedná o 19725 fotografií. Výhodou je, že dataset obsahuje kromě původních fotografií celých stránek také anotované výřezy řádků, kterých je více než 500 tisíc. Sada je rozdělena na na tři podmnožiny - trénovací, validační a testovací. Ty jsou ještě rozděleny do tří kategorií podle obtížnosti na lehkou, střední a těžkou. Fotografie mají různé úrovně ostrosti, světelnosti, atd. 

Podobnou datovou sadou je SmartDoc\cite{smart-doc} publikovaný v rámci soutěže ICDAR v roce 2015. Obsahuje 12100 fotografií tištěného textu pořízených dvěma mobilními telefony. Má však několik nevýhod - fotografie jsou pořízeny v přesně definovaných podmínkách, takže jsou poměrně kvalitní a tvoří proto pro současné systémy poměrně jednoduchou úlohu. Další nevýhodou je malá velikost v porovnání s Brno Mobile OCR. Další nevýhodou je pak to, že dataset nenabízí anotované výřezy, ale pouze fotografie celých stránek.

Dále bychom do kategorie tištěných textů mohli zařadit i dataset IMPACT\cite{impact}, který se ovšem zaměřuje především na historické texty. Shromažďuje texty od 15. století do současnosti. Některé z nich jsou ručně psané, ale většina je tištěných. Výhodou této sady je její velikost a rozmanitost - 600000 obrázků z celkem deseti národních knihoven z celé Evropy. Některé z obrázků mají anotované rozvržení stránky (hlavička, odstavce, obrázky, apod.), některé pak dokonce anotované výřezy řádků. Tyto výřezy řádků jsou však dostupné pouze pro 300 obrázků, tedy výrazně méně než u Brno Mobile OCR.

\textbf{Formát dat.} Jak již bylo zmíněno, použili jsme datovou sadu Brno Mobile OCR. Ta obsahuje fotografie formátu \textit{jpg} o výšce 48 pixelů a proměnlivé šířce, které zobrazují vyřezané řádky tištěného textu. Dále obsahuje anotace k těmto řádkům (\textit{ground truth}).

\begin{figure}[H]
\center{\includegraphics[width=\textwidth/2]{line.jpg}}
\caption{\label{fig:line} Příklad vyříznutého řádku z datové sady Brno Mobile OCR. \textit{Ground truth} k takovémuto obrázku pak bude řetězec "on-board the USS CORONADO; a deployable CMOC"}
\end{figure}

\section*{Řešení}
Na základě informací prezentovaných v kapitole pojednávající o existujících přístupech jsme se rozhodli pro neuronovou síť založenou na CTC \cite{ctc-origin}. Jedná se tedy o architekturu inspirovanou článkem od kolektivu autorů Baoguang Shi, Xiang Bai, Cong Yao \cite{end-to-end-ocr}. \\
Drželi jsme se "ověřeného přístupu", kde v první řadě probíhá extrakce příznaků za pomoci konvolučních vrstev a poolingu a následně jsou příznaky zpracovány v rekurentních vrstvách. Poté jsou zpracovány v závěrečných konvolučních vrstvách. Výsledný vektor hodnot, který má délku odpovídající velikosti vstupní abecedy, je pomocí funkce \textit{logsoftmax} převeden na vektor pravděpodobností pro jednotlivé znaky. Vše je završené již zmiňovanou loss funkcí CTC. Finálními metrikami úspěšnosti našeho OCR systému budou metriky CER.
\\
Řešení bylo implementováno za použití frameworku PyTorch a jako datová sada posloužilo Brno Mobile OCR\cite{brno-mobile-ocr} z důvodů prezentovaných v předchozí kapitole.\\

\begin{figure}[H]
\center{\includegraphics[width=\textwidth/2]{architecture.png}}
\caption{\label{fig:architecture} Architektura CNN, která posloužila jako inspirace při vytváření vlastní NN. Převzato z \cite{end-to-end-ocr}}
\end{figure}

\section*{Experimenty}
V rámci řešení jsme experimentovali s různými architekturami sítí, včetně využití předtrénovaných síťí z Pytorch model zoo, a i s rozličným pojetím variabily výšky vstupního obrázku. Přičemž nejlepší výsledky a zároveň rychlé trénování přinesla architektura kombinující konvoluční a rekurentní vrstvy, která je popsána v tabulce \ref{tab:architecture}.
\par
\begin{table}[]
    \centering
    \begin{tabular}{|l|}
\hline
Conv2d(ch=3:32, k:3x3, p:1) \\ \hline
ReLU \\ \hline
MaxPool2d \\ \hline
Conv2d(ch=32:64, k:3x3, p:1) \\ \hline
BatchNorm2d \\ \hline
ReLU \\ \hline
MaxPool2d \\ \hline
Conv2d(ch=64:128, k:3x3, p:1, s:(3, 1)) \\ \hline
BatchNorm2d \\ \hline
ReLU \\ \hline
MaxPool2d \\ \hline
bidirectional LSTM(inSize:128, hSize:128) \\ \hline
Conv1d(ch=256:91, k:3x3, p:1) \\ \hline
LogSoftmax \\ \hline
    \end{tabular}
    \caption{Architektura použité sítě.}
    \label{tab:architecture}
\end{table}

\par
Finální trénování sítě bylo provedeno na celé trénovací části datasetu a následné byla síť otestována na validačních datech. Přehled dosažených výsledků pro různě obtížné příklady z validační sady, je zobrazen v tabulce \ref{tab:results}.

\begin{table}[]
    \centering
    \begin{tabular}{l c c }
         Obtížnost & CER trénovací & CER validační  \\
         \hline
         easy & 2.4  & 2.9 \\
         medium & 11.3 & 16.7  \\
         hard & 36.1 & 39.8 \\ 
         \hline
         all & 5.05 & 7.5 \\
    \end{tabular}
    \caption{Výsledky trénovaní sítě}
    \label{tab:results}
\end{table}
\par
Z výsledků vyplývá poměrně dobrá činnost sítě jak na trénovací tak i na finálně testovaných validačních datech, kde se sice ukázalo malé přetrénovaní sítě, které se v takto malé míře ale očekávalo. Zároveň je z výsledků patrná podstatně horší schopnost přepisu, což je očekávaný výsledek, který by se dal pravděpodobně zlepšit využitím technik augmentace trénovacích dat. Malá vizualizace výsledků je zobrazena v obrázku \ref{fig:results}.


\begin{figure}
\begin{subfigure}{0.4\textwidth}
  \centering
  % include first image
  \includegraphics[width=.8\linewidth]{07a3031318e4d2fbff69f3f04c32b958.jpg_rec_l0011.jpg}  
  \caption{Ground truth: which they are not. \\ výstup sítě: which they are not.}

\end{subfigure}
\newline
\begin{subfigure}{0.4\textwidth}
  \centering
  % include first image
  \includegraphics[width=.8\linewidth]{f7e1417cf800919dc61672494d0adb4e.jpg_rec_l0037.jpg}  
  \caption{Ground truth: primitive data, \\ výstup sítě: prinitive data,}
\end{subfigure}
\newline
\begin{subfigure}{0.4\textwidth}
  \centering
  % include first image
  \includegraphics[width=.8\linewidth]{4a989e18dade24876157c8f3c0e50d3d.jpg_rec_l0089.jpg}  
  \caption{Ground truth: NY: Macmillan, 1994. \\ výstup sítě: NO Macealla. 1999.}
\end{subfigure}
\caption{Ukázky vstupů a výstupů sítě.}
\label{fig:results}
\end{figure}

\section*{Závěr}
V rámci tohoto projektu byly shrnuty poznatky týkající se úlohy optického rozpoznávání znaků a byl představen systém na bázi neuronových sítí řešící tuto úlohu. Úspěšnost vytvořeného systému na datasetu Brno Mobile OCR je kolem 7\% CER. Na práci by bylo možné navázat vyzkoušením jiných architektur neuronových sítí nebo například přidáním augmentace dat a regularizací sítě.  


\newpage

\renewcommand{\refname}{Literatura}
\bibliographystyle{czechiso}
\bibliography{citations}


\end{document}